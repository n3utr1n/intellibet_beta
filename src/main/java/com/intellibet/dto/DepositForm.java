package com.intellibet.dto;

import lombok.Data;

@Data
public class DepositForm {

  private String depositAmount;
  private String balance;

}
